﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Threading;
namespace Lab1_experiment
{
    class Program
    {
        class TestClass
        { 
            public void TestMethod() { }
        }

        static void TestMethodCallDefault()
        {
            var testClass = new TestClass();
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            for (int i = 0; i < 10_000_000; i++)
            {
                testClass.TestMethod();
            }
            Console.WriteLine($"Time of TestMethodCallDefault: {stopWatch.ElapsedMilliseconds}ms");
        }
        static void TestMethodCallReflection()
        {
            var testClass = new TestClass();
            Stopwatch stopWatch = new Stopwatch();
            MethodInfo myMethod = testClass.GetType().GetMethod("TestMethod");
            stopWatch.Start();
            for (int i = 0; i < 10_000_000; i++)
            {
                myMethod.Invoke(testClass, null);
            }
            Console.WriteLine($"Time of TestMethodCallReflection: {stopWatch.ElapsedMilliseconds}ms");
        }

        static void Main(string[] args)
        {
            for (int i = 0; i < 5; i++)
            {
                ConsoleColor prewFore = Console.ForegroundColor;
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine($"Experiment {i + 1}:");
                Console.ForegroundColor = prewFore;
                TestMethodCallDefault();
                TestMethodCallReflection();
            }
            Console.ReadLine();
        }
    }
}
